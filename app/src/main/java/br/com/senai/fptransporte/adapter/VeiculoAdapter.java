package br.com.senai.fptransporte.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.senai.fptransporte.R;
import br.com.senai.fptransporte.holder.VeiculoViewHolder;
import br.com.senai.fptransporte.model.Veiculo;

/**
 * Created by 36315014801 on 25/04/2018.
 */

public class VeiculoAdapter extends RecyclerView.Adapter {

    private final Context context;
    private final List<Veiculo> veiculos;

    public VeiculoAdapter(Context context, List<Veiculo> veiculos){
        this.context = context;
        this.veiculos = veiculos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_veiculos, parent, false);
        VeiculoViewHolder holder = new VeiculoViewHolder(view, this);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        VeiculoViewHolder meuHolder = (VeiculoViewHolder) holder;
        Veiculo veiculo = veiculos.get(position);
        meuHolder.preencher(veiculo);
    }

    @Override
    public int getItemCount() {
        return veiculos.size();
    }
}
