package br.com.senai.fptransporte;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.time.LocalDate;

import br.com.senai.fptransporte.model.Locacao;
import br.com.senai.fptransporte.model.Veiculo;

/**
 * Created by 36315014801 on 27/04/2018.
 */

public class FormularioHelperLocar  {


    public EditText nomeLocar, passageirosParaLocar, origemLocar, destinoLocar;
    public TextView marcaLocar, dataSaidaLocar, dataEntradaLocar, anoLocar,precoLocar, passageirosLocar, modeloLocar, textResultado;
    public CheckBox cbkMotorista;
    public ImageView imagemId;

    public Locacao locacao;
    public Veiculo veiculo;


    public FormularioHelperLocar(FormularioLocarActivity formularioLocar) {
        nomeLocar = formularioLocar.findViewById(R.id.editLocarNome);
        passageirosParaLocar = formularioLocar.findViewById(R.id.editLocarPassageiros);
        dataSaidaLocar = formularioLocar.findViewById(R.id.editLocarDataSaida);
        dataEntradaLocar = formularioLocar.findViewById(R.id.editLocarDataEntrada);
        origemLocar = formularioLocar.findViewById(R.id.editLocarOrigem);
        destinoLocar = formularioLocar.findViewById(R.id.editLocarDestino);
        textResultado = formularioLocar.findViewById(R.id.txtResultado);
        //cbkMotorista = formularioLocar.findViewById(R.id.cbMotorista);

        marcaLocar = formularioLocar.findViewById(R.id.txtGetMarca);
        anoLocar = formularioLocar.findViewById(R.id.txtGetAno);
        precoLocar = formularioLocar.findViewById(R.id.txtGetPreco);
        passageirosLocar = formularioLocar.findViewById(R.id.txtGetPassageiros);
        modeloLocar = formularioLocar.findViewById(R.id.txtGetModelo);
        imagemId = formularioLocar.findViewById(R.id.imgGetLocar);
        locacao = new Locacao();

    }
    public Locacao pegaLocacao() {

        locacao.setNome(nomeLocar.getText().toString());
        locacao.setNumeroPassageiros(passageirosParaLocar.getText().toString());
        locacao.setDataSaida(dataSaidaLocar.getText().toString());
        locacao.setDataEntrada(dataEntradaLocar.getText().toString());
        locacao.setOrigem(origemLocar.getText().toString());
        locacao.setDestino(destinoLocar.getText().toString());
        locacao.setPrecoLocacao(textResultado.getText().toString());
        //locacao.setMotorista(cbkMotorista.getText().toString());
        return locacao;
    }

    public void preecherFormulario(Veiculo veiculo) {
        marcaLocar.setText(veiculo.getMarca());
        anoLocar.setText(veiculo.getAno());
        precoLocar.setText(veiculo.getPreco());
        passageirosLocar.setText(veiculo.getNumeroDePassageiros());
        modeloLocar.setText(veiculo.getModelo());
        String caminhoImagem = veiculo.getImagemVeiculo();
        Bitmap imagemCarregada = BitmapFactory.decodeFile(caminhoImagem);
        Bitmap imagemReduzida =
                Bitmap.createScaledBitmap(imagemCarregada, 150, 150, true);
        imagemId.setImageBitmap(imagemReduzida);

        this.veiculo = veiculo;
    }

}
