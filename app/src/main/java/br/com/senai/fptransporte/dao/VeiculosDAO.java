package br.com.senai.fptransporte.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senai.fptransporte.model.Veiculo;

/**
 * Created by 36315014801 on 25/04/2018.
 */

public class VeiculosDAO extends SQLiteOpenHelper {

    public VeiculosDAO(Context context) {
        super(context, "VeiculosDBT", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Veiculos(id INTEGER PRIMARY KEY, marca TEXT, caminhoImagem TEXT, ano TEXT, modelo TEXT, preco text, numeroDePassageiros TEXT)";
        db.execSQL(sql);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Veiculos";
        db.execSQL(sql);
    }

    public void insere(Veiculo veiculo) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = new ContentValues();
        dados.put("ano", veiculo.getAno());
        dados.put("marca", veiculo.getMarca());
        dados.put("modelo", veiculo.getModelo());
        dados.put("preco", veiculo.getPreco());
        dados.put("numeroDePassageiros", veiculo.getNumeroDePassageiros());
        dados.put("caminhoImagem", veiculo.getImagemVeiculo());
        db.insert("Veiculos", null, dados);
    }

    public List<Veiculo> buscarVeiculos() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM Veiculos";
        List<Veiculo> veiculos = new ArrayList<>();
        //executando
        Cursor c = db.rawQuery(sql,null);
        while (c.moveToNext()){
            Veiculo veiculo = new Veiculo();
            veiculo.setId(c.getLong(c.getColumnIndex("id")));
            veiculo.setAno(c.getString(c.getColumnIndex("ano")));
            veiculo.setMarca(c.getString(c.getColumnIndex("marca")));
            veiculo.setModelo(c.getString(c.getColumnIndex("modelo")));
            veiculo.setPreco(c.getString(c.getColumnIndex("preco")));
            veiculo.setNumeroDePassageiros(c.getString(c.getColumnIndex("numeroDePassageiros")));
            veiculo.setImagemVeiculo(c.getString(c.getColumnIndex("caminhoImagem")));
            veiculos.add(veiculo);
        }
        c.close();
        return veiculos;
    }

    public Veiculo localizar(Long veiculoId) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "SELECT * from Veiculos WHERE id = ?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(veiculoId)});
        c.moveToFirst();
        Veiculo veiculoRetornado = new Veiculo();
        veiculoRetornado.setId(c.getLong(c.getColumnIndex("id")));
        veiculoRetornado.setAno(c.getString(c.getColumnIndex("ano")));
        veiculoRetornado.setMarca(c.getString(c.getColumnIndex("marca")));
        veiculoRetornado.setModelo(c.getString(c.getColumnIndex("modelo")));
        veiculoRetornado.setPreco(c.getString(c.getColumnIndex("preco")));
        veiculoRetornado.setNumeroDePassageiros(c.getString(c.getColumnIndex("numeroDePassageiros")));
        veiculoRetornado.setImagemVeiculo(c.getString(c.getColumnIndex("caminhoImagem")));
        db.close();
        return veiculoRetornado;
    }


}
