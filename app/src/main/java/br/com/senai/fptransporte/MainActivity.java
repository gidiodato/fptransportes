package br.com.senai.fptransporte;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button botaoCadastrar;
    private Button botaoLocar;
    private Button botaoLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botaoCadastrar = findViewById(R.id.btnCadastar);
        botaoLocar = findViewById(R.id.btnLocar);

        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, FormularioVeiculoActivity.class);
                startActivity(intent);
            }
        });

        botaoLocar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListaVeiculos.class);
                startActivity(intent);
            }
        });

    }

}
