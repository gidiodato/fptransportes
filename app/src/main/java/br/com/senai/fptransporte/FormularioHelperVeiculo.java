package br.com.senai.fptransporte;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.senai.fptransporte.model.Veiculo;

/**
 * Created by 36315014801 on 25/04/2018.
 */

public class FormularioHelperVeiculo {


    private ImageView imagemSelecionada;
    private EditText editTextModelo;
    private EditText editTextAno;
    private EditText editTextMarca;
    private EditText editTextPreco;
    private EditText editTextPassageiros;
    private Veiculo veiculo;
    private Button cadastrar;


    public ImageView getImagemSelecionada() {
        return imagemSelecionada;
    }

    public void setImagemSelecionada(ImageView imagemSelecionada) {
        this.imagemSelecionada = imagemSelecionada;
    }

    public EditText getEditTextPreco() {
        return editTextPreco;
    }

    public void setEditTextPreco(EditText editTextPreco) {
        this.editTextPreco = editTextPreco;
    }

    public Button getCadastrar() {
        return cadastrar;
    }

    public FormularioHelperVeiculo(FormularioVeiculoActivity formulario) {
        veiculo = new Veiculo();
        imagemSelecionada = formulario.findViewById(R.id.imgCadastrarVeiculo);
        editTextModelo = formulario.findViewById(R.id.editModelo);
        editTextAno = formulario.findViewById(R.id.editAno);
        editTextMarca = formulario.findViewById(R.id.editMarca);
        editTextPreco = formulario.findViewById(R.id.editPreco);
        editTextPassageiros = formulario.findViewById(R.id.editPassageiros);
        cadastrar = formulario.findViewById(R.id.btnCadastrarVeiculo);
    }

    public Veiculo pegaVeiculo() {
        veiculo.setImagemVeiculo(imagemSelecionada.getTag().toString());
        veiculo.setAno(editTextAno.getText().toString());
        veiculo.setModelo(editTextModelo.getText().toString());
        veiculo.setMarca(editTextMarca.getText().toString());
        veiculo.setPreco(editTextPreco.getText().toString());
        veiculo.setNumeroDePassageiros(editTextPassageiros.getText().toString());
        return veiculo;
    }


}