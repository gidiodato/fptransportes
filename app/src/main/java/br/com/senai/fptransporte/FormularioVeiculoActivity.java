package br.com.senai.fptransporte;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import br.com.senai.fptransporte.dao.LocacoesDAO;
import br.com.senai.fptransporte.dao.VeiculosDAO;
import br.com.senai.fptransporte.model.Veiculo;

public class FormularioVeiculoActivity extends AppCompatActivity {

    public static final int CODIGO_GALERIA = 1;
    private ImageView abrirGaleria;
    //private EditText textModelo;
    //private EditText textAno;
    //private EditText textMarca;
    //private EditText textPassageiros;
    //private EditText textPreco;
    private Button botaoCadastrar;
    private FormularioHelperVeiculo helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_veiculo);
        helper = new FormularioHelperVeiculo(this);

        abrirGaleria = helper.getImagemSelecionada();
        botaoCadastrar = helper.getCadastrar();

        abrirGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, CODIGO_GALERIA);
            }
        });
        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Veiculo veiculo = helper.pegaVeiculo();
                if (validar(veiculo) != null){
                    Toast.makeText(getApplicationContext(),validar(veiculo),Toast.LENGTH_LONG).show();
                }else {
                    VeiculosDAO dao = new VeiculosDAO(FormularioVeiculoActivity.this);
                    dao.insere(veiculo);
                    Toast.makeText(getApplicationContext(),"Veiculo cadastrado",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(FormularioVeiculoActivity.this, ListaVeiculos.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }

    }//FIM ONCREATE

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // / A permissão foi concedida. Pode continuar

            } else{
                // A permissão foi negada. Precisa ver o que deve ser desabilitado
            }
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CODIGO_GALERIA){
            Uri uri = data.getData();
            String[] diretorios = {MediaStore.Images.Media.DATA};
            Cursor c = getContentResolver().query(uri, diretorios, null,null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(diretorios[0]);
            String caminhoDaImagem = c.getString(columnIndex);
            c.close();
            Bitmap imagemRetornada= BitmapFactory.decodeFile(caminhoDaImagem);
            abrirGaleria.setImageBitmap(imagemRetornada);
            abrirGaleria.setTag(caminhoDaImagem);

        }
    }

    public String validar(Veiculo veiculo){
        String mensagem ;

        if (veiculo.getModelo() == null || veiculo.getModelo().isEmpty()){
            mensagem = "O campo modelo é obrigatório";
            return mensagem;
        } else if (veiculo.getAno() == null || veiculo.getAno().isEmpty()){
            mensagem = "O campo ano é obrigatório";
            return mensagem;
        } else if (veiculo.getMarca() == null || veiculo.getMarca().isEmpty()){
            mensagem = "O campo marca é obrigatório";
            return mensagem;
        } else if (veiculo.getNumeroDePassageiros() == null || veiculo.getNumeroDePassageiros().isEmpty()){
            mensagem = "O campo numero de passageiros é obrigatório";
            return mensagem;
        }else if (veiculo.getPreco() == null || veiculo.getPreco().isEmpty()){
            mensagem = "O campo preco é obrigatório";
            return mensagem;
        }

        return null;
    }
}
