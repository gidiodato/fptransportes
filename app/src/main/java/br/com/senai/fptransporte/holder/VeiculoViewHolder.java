package br.com.senai.fptransporte.holder;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.senai.fptransporte.FormularioLocarActivity;
import br.com.senai.fptransporte.R;
import br.com.senai.fptransporte.adapter.VeiculoAdapter;
import br.com.senai.fptransporte.model.Veiculo;

/**
 * Created by 36315014801 on 25/04/2018.
 */

public class VeiculoViewHolder extends RecyclerView.ViewHolder{

    private final VeiculoAdapter adapter;
    private ImageView imagem;
    private TextView modelo;
    private Long veiculoId;


public VeiculoViewHolder(View itemView, VeiculoAdapter adapter) {
        super(itemView);
        this.adapter = adapter;

        imagem = itemView.findViewById(R.id.imgId);
        modelo = itemView.findViewById(R.id.modeloId);

    itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final Activity context = (Activity) view.getContext();
            final Intent intent = new Intent(context, FormularioLocarActivity.class);
            intent.putExtra("veiculoId", veiculoId);
            context.startActivityForResult(intent,1);
        }
    });

        }

public void preencher(Veiculo veiculo) {
        veiculoId = veiculo.getId();
        modelo.setText(veiculo.getModelo());
        String caminhoImagem = veiculo.getImagemVeiculo();
        Bitmap imagemCarregada = BitmapFactory.decodeFile(caminhoImagem);
        Bitmap imagemReduzida =
        Bitmap.createScaledBitmap(imagemCarregada, 150, 150, true);
        imagem.setImageBitmap(imagemReduzida);

        }
        }
