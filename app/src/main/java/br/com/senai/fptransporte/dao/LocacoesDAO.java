package br.com.senai.fptransporte.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senai.fptransporte.model.Locacao;

/**
 * Created by 36315014801 on 27/04/2018.
 */

public class LocacoesDAO extends SQLiteOpenHelper {

    public LocacoesDAO(Context context) {
        super(context, "LocacoesDBTG", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Locacoes(id INTEGER PRIMARY KEY, nome TEXT, numeroPassageiros TEXT, dataEntrada TEXT, dataSaida TEXT, origem text, destino TEXT, motorista TEXT, precoLocacao TEXT)";
        db.execSQL(sql);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Locacoes";
        db.execSQL(sql);
    }

    public void insere(Locacao locacao) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = new ContentValues();
        dados.put("nome", locacao.getNome());
        dados.put("numeroPassageiros", locacao.getNumeroPassageiros());
        dados.put("dataEntrada", locacao.getDataEntrada());
        dados.put("dataSaida", locacao.getDataSaida());
        dados.put("origem", locacao.getOrigem());
        dados.put("destino", locacao.getDestino());
        dados.put("motorista", locacao.getMotorista());
        dados.put("precoLocacao", locacao.getPrecoLocacao());
        db.insert("Locacoes", null, dados);
    }

    public List<Locacao> buscarLocacoes() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM Locacoes";
        List<Locacao> locacaos = new ArrayList<>();
        //executando
        Cursor c = db.rawQuery(sql,null);
        while (c.moveToNext()){
            Locacao locacao = new Locacao();
            locacao.setId(c.getLong(c.getColumnIndex("id")));
            locacao.setNome(c.getString(c.getColumnIndex("nome")));
            locacao.setNumeroPassageiros(c.getString(c.getColumnIndex("numeroPassageiros")));
            locacao.setDataEntrada(c.getString(c.getColumnIndex("dataEntrada")));
            locacao.setDataSaida(c.getString(c.getColumnIndex("dataSaida")));
            locacao.setOrigem(c.getString(c.getColumnIndex("origem")));
            locacao.setDestino(c.getString(c.getColumnIndex("destino")));
            locacao.setMotorista(c.getString(c.getColumnIndex("motorista")));
            locacao.setPrecoLocacao(c.getString(c.getColumnIndex("precoLocacao")));
            locacaos.add(locacao);
        }
        c.close();
        return locacaos;
    }
}
