package br.com.senai.fptransporte;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import br.com.senai.fptransporte.dao.LocacoesDAO;
import br.com.senai.fptransporte.dao.VeiculosDAO;
import br.com.senai.fptransporte.model.Locacao;
import br.com.senai.fptransporte.model.Veiculo;

public class FormularioLocarActivity extends AppCompatActivity {

    private CheckBox cbkMotorista;
    public TextView dataSaidaLocar, dataEntradaLocar, textResultado;
    private String dateEntrada, dateSaida;
    private VeiculosDAO dao;
    private Button botaoLocar, botaoPreco;
    private DatePickerDialog.OnDateSetListener  escutadorDeData, escutadorDeDataSaida;
    private LocalDate dataEntrada, dataSaida;
    //private Veiculo veiculo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_locar);

        dao = new VeiculosDAO(this);

        final FormularioHelperLocar helper = new FormularioHelperLocar(this);

        Bundle extras = getIntent().getExtras();
        Long veiculoId = (extras != null)? extras.getLong("veiculoId"): null;

        final Veiculo veiculo = dao.localizar(veiculoId);
        helper.preecherFormulario(veiculo);

        botaoLocar = findViewById(R.id.btnLocar);
        dataEntradaLocar = findViewById(R.id.editLocarDataEntrada);
        dataSaidaLocar = findViewById(R.id.editLocarDataSaida);
        botaoPreco = findViewById(R.id.btnPreco);
        cbkMotorista = findViewById(R.id.cbMotorista);
        textResultado = findViewById(R.id.txtResultado);

        botaoLocar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Locacao locacao = helper.pegaLocacao();

                if (validar(locacao) != null){
                    Toast.makeText(getApplicationContext(),validar(locacao),Toast.LENGTH_LONG).show();
                }else {
                    LocacoesDAO dao = new LocacoesDAO(FormularioLocarActivity.this);
                    dao.insere(locacao);
                    Intent intent = new Intent(FormularioLocarActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        dataEntradaLocar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        FormularioLocarActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        escutadorDeData,
                        year,
                        month,
                        day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }//fim onClick
        });//Fim OnClickListener

        dataSaidaLocar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        FormularioLocarActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        escutadorDeDataSaida,
                        year,
                        month,
                        day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }//fim onClick
        });//Fim OnClickListener

        escutadorDeData = new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month+1;
                dateEntrada = dayOfMonth+"/"+month+"/"+year;
                dataEntradaLocar.setText(dateEntrada);
                dataEntrada = LocalDate.of(year, month, dayOfMonth);

            }
        };

        escutadorDeDataSaida = new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month+1;
                dateSaida = dayOfMonth+"/"+month+"/"+year;
                dataSaidaLocar.setText(dateSaida);
                dataSaida = LocalDate.of(year, month, dayOfMonth);
            }
        };

        botaoPreco.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Long dif = dataEntrada.until(dataSaida, ChronoUnit.DAYS);
                Double precoTotal = Double.parseDouble(veiculo.getPreco()) * dif ;
                if(cbkMotorista.isChecked()){
                   Double precoMotorista = precoTotal + 20.00;
                    //Toast.makeText(getApplicationContext(),"Preco Total: "+precoMotorista,Toast.LENGTH_LONG).show();
                    textResultado.setText(String.valueOf(precoMotorista));
                }else{
                   // Toast.makeText(getApplicationContext(), "Preco Total: " + precoTotal, Toast.LENGTH_LONG).show();
                    textResultado.setText(String.valueOf(precoTotal));
                }
            }
        });


    }

    public String validar(Locacao locacao){
        String mensagem ;

        if (locacao.getNome() == null || locacao.getNome().isEmpty()){
            mensagem = "O campo nome é obrigatório";
            return mensagem;
        } else if (locacao.getNumeroPassageiros() == null || locacao.getNumeroPassageiros().isEmpty()){
            mensagem = "O campo numero de passageiros é obrigatório";
            return mensagem;
        } else if (locacao.getDestino() == null || locacao.getDestino().isEmpty()){
            mensagem = "O campo destino é obrigatório";
            return mensagem;
        } else if (locacao.getOrigem() == null || locacao.getOrigem().isEmpty()){
            mensagem = "O campo origem é obrigatório";
            return mensagem;
        }

        return null;
    }
}
