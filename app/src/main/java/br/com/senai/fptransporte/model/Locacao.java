package br.com.senai.fptransporte.model;

/**
 * Created by 36315014801 on 27/04/2018.
 */

public class Locacao {
    private Long id;
    private String nome;
    private String numeroPassageiros;
    private String dataEntrada;
    private String dataSaida;
    private String origem;
    private String destino;
    private String motorista;
    private String precoLocacao;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroPassageiros() {
        return numeroPassageiros;
    }

    public void setNumeroPassageiros(String numeroPassageiros) {
        this.numeroPassageiros = numeroPassageiros;
    }

    public String getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(String dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public String getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(String dataSaida) {
        this.dataSaida = dataSaida;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getMotorista() {
        return motorista;
    }

    public void setMotorista(String motorista) {
        this.motorista = motorista;
    }

    public String getPrecoLocacao() {
        return precoLocacao;
    }

    public void setPrecoLocacao(String precoLocacao) {
        this.precoLocacao = precoLocacao;
    }
}
