package br.com.senai.fptransporte;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import java.util.List;

import br.com.senai.fptransporte.adapter.VeiculoAdapter;
import br.com.senai.fptransporte.dao.VeiculosDAO;
import br.com.senai.fptransporte.model.Veiculo;

public class ListaVeiculos extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private RecyclerView listaDeVeiculos;
    private VeiculosDAO dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_veiculos);

        listaDeVeiculos = findViewById(R.id.listaDeVeiculos);


        dao = new VeiculosDAO(this);

        carregarLista(dao.buscarVeiculos());

    }

    private void carregarLista(List<Veiculo> veiculos) {
        VeiculoAdapter adapter = new VeiculoAdapter(this, veiculos);
        listaDeVeiculos.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listaDeVeiculos.setLayoutManager(layoutManager);

    }



    @Override
    public boolean onQueryTextSubmit(String query) {
        //Dispara ação quando o conteudo for submetido.
        Toast.makeText(getApplicationContext(),"Valor enviado:"+ query,Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //Dispara ação conforeme é digitado no campo
        Toast.makeText(getApplicationContext(),"Texto Digitado"+newText,Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        carregarLista(dao.buscarVeiculos());
    }
}
